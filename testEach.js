const each = require('./each');

const items = [1, 2, 3, 4, 5, 5];
    //elements   //callback function
each(items, function(element, index) {
    console.log(`Element ${element} at index ${index}`);
});
