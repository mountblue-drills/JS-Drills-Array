function each(elements, callBack) {                     
    for (let i = 0; i < elements.length; i++) {
        callBack(elements[i], i);
    }
}

module.exports = each;
